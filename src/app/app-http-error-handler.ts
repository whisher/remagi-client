import { environment } from '../environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

export class HttpErrorHandler {
  static handle(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage =  error.error.message;
    } else {
      const errorMessage = `Server returned code: ${error.status}, error message is: ${error.error}`;
    }
    const msg = true ? 'Something bad happened; please try again later.' : errorMessage;
    return throwError(msg);
  }
}
