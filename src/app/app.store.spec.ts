import { TestBed, inject } from '@angular/core/testing';

import { AppStore } from './app.store';

describe('StoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppStore]
    });
  });

  it('should be created', inject([AppStore], (service: AppStore) => {
    expect(service).toBeTruthy();
  }));
});
