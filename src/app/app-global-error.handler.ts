import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { throwError } from 'rxjs';

import { environment } from '../environments/environment';

@Injectable()
export class AppGlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) { }

  handleError(error) {
    if (error instanceof HttpErrorResponse) {
      if (!navigator.onLine) {
        throwError('Off line');
      } else {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          errorMessage =  error.error.message;
        } else {
          const errorMessage = `Server returned code: ${error.status}, error message is: ${error.error}`;
        }
        const msg = environment.production ? 'Something bad happened; please try again later.' : errorMessage;
        return throwError(msg);
      }
    }
    throw error;
  }

}
