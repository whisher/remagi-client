import { DefaultRoutingModule } from './default-routing.module';

describe('DefaultRoutingModule', () => {
  let defaultRoutingModule: DefaultRoutingModule;

  beforeEach(() => {
    defaultRoutingModule = new DefaultRoutingModule();
  });

  it('should create an instance', () => {
    expect(defaultRoutingModule).toBeTruthy();
  });
});
