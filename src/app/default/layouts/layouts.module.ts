import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from '../../app-shared.module';

import * as fromContainers from './containers';

@NgModule({
  imports: [
    RouterModule,
    AppSharedModule
  ],
  declarations: [...fromContainers.containers]
})
export class DefaultLayoutsModule { }
