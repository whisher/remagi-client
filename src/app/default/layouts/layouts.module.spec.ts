import { DefaultLayoutsModule } from './layouts.module';

describe('LayoutsModule', () => {
  let layoutsModule: DefaultLayoutsModule;

  beforeEach(() => {
    layoutsModule = new DefaultLayoutsModule();
  });

  it('should create an instance', () => {
    expect(layoutsModule).toBeTruthy();
  });
});
