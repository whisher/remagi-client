import { DefaultFooterComponent } from './footer/footer.component';
import { DefaultHeaderComponent } from './header/header.component';
import { DefaultMainComponent } from './main/main.component';

export const containers: any[] = [
  DefaultFooterComponent,
  DefaultHeaderComponent,
  DefaultMainComponent
];

export * from './footer/footer.component';
export * from './header/header.component';
export * from './main/main.component';
