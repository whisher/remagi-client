import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultMainComponent } from './main.component';

describe('MainComponent', () => {
  let component: DefaultMainComponent;
  let fixture: ComponentFixture<DefaultMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
