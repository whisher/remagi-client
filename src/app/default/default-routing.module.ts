import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultMainComponent } from './layouts/containers';
import { DefaultHomeComponent} from './containers';



export const ROUTES: Routes = [
  {
    path: '', component: DefaultMainComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      { path: 'home', component: DefaultHomeComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class DefaultRoutingModule { }
