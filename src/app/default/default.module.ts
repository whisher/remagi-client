import { NgModule } from '@angular/core';

import { DefaultLayoutsModule } from './layouts/layouts.module';
import { DefaultRoutingModule } from './default-routing.module';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    DefaultLayoutsModule,
    DefaultRoutingModule
  ],
  declarations: [...fromContainers.containers]
})
export class DefaultModule { }
