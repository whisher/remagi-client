import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationNavbarComponent } from './navbar.component';

describe('AccountNavbarComponent', () => {
  let component: AuthenticationNavbarComponent;
  let fixture: ComponentFixture<AuthenticationNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
