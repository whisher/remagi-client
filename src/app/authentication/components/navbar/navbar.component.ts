import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class AuthenticationNavbarComponent {
  @Input() isValid : boolean;
  @Output()
  next: EventEmitter<boolean> = new EventEmitter();
  @Output()
  back: EventEmitter<boolean> = new EventEmitter();

  onSetData(){
    this.next.emit(true);
  }

  onBack(){
    this.back.emit(true);
  }

}
