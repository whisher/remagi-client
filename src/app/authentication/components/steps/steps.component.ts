import { Component, Input, DoCheck, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class AuthenticationStepsComponent implements DoCheck {

  steps: Array<number>;
  @Input() numOfSteps: number;
  @Input() step: number;
  constructor() { }

  ngDoCheck() {
    this.steps = Array(this.numOfSteps).fill(null).map((x,i)=>i);
  }

}
