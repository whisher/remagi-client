import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationStepsComponent } from './steps.component';

describe('AuthenticationStepsComponent', () => {
  let component: AuthenticationStepsComponent;
  let fixture: ComponentFixture<AuthenticationStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
