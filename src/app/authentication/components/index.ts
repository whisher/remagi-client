import { AuthenticationNavbarComponent } from './navbar/navbar.component';
import { AuthenticationStepsComponent } from './steps/steps.component';

export const components: any[] = [
  AuthenticationNavbarComponent,
  AuthenticationStepsComponent
];

export * from './navbar/navbar.component';
export * from './steps/steps.component';
