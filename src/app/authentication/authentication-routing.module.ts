import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationMainComponent } from './layouts/containers';

import {
  AuthenticationAccessInfoComponent,
  AuthenticationAccountInfoComponent,
  AuthenticationBasicInfoComponent,
  AuthenticationCompanyInfoComponent,
  AuthenticationForgotPasswordComponent,
  AuthenticationLoginComponent,
  AuthenticationLogoutComponent,
  AuthenticationWhoAreYouComponent } from './containers';

import {
  MemoryGuard,
  LoginGuard } from './guards';


export const ROUTES: Routes = [
  {
    path: '', component: AuthenticationMainComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login' },
      { path: 'accessinfo', component: AuthenticationAccessInfoComponent, canActivate: [ MemoryGuard ], data: {animation: {page: 'accessinfo'}} },
      { path: 'accountinfo', component: AuthenticationAccountInfoComponent, canActivate: [ MemoryGuard ], data: {animation: {page: 'accountinfo'}} },
      { path: 'basicinfo', component: AuthenticationBasicInfoComponent, canActivate: [ MemoryGuard ], data: {animation: {page: 'basicinfo'}} },
      { path: 'companyinfo', component: AuthenticationCompanyInfoComponent, canActivate: [ MemoryGuard ], data: {animation: {page: 'companyinfo'}} },
      { path: 'forgotpassword', component: AuthenticationForgotPasswordComponent, canActivate: [ LoginGuard ], data: {animation: {page: 'forgotpassword'}} },
      { path: 'login', component: AuthenticationLoginComponent, canActivate: [ LoginGuard ], data: {animation: {page: 'login'}} },
      { path: 'whoareyou', component: AuthenticationWhoAreYouComponent, data: {animation: {page: 'whoareyou'}} },
    ],
  },
  { path: 'logout', component: AuthenticationLogoutComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  providers: [
    LoginGuard,
    MemoryGuard
  ],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
