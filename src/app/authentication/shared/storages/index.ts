import { AccountSharedStorage } from './account.storage';

import { AuthenticationSharedStorage } from './authentication.storage';

export const storages: any[] = [
  AccountSharedStorage,
  AuthenticationSharedStorage
];

export * from './account.storage';
export *  from './authentication.storage';
