import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationSharedStorage } from './authentication.storage';

describe('AuthenticationSharedStorage', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationSharedStorage]
    });
  });

  it('should be created', inject([AuthenticationSharedStorage], (service: AuthenticationSharedStorage) => {
    expect(service).toBeTruthy();
  }));
});
