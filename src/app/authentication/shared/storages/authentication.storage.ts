import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Observable } from 'rxjs';
import { AuthenticationToken } from '../models/authentication.model';

@Injectable()
export class AuthenticationSharedStorage {

  private key = 'authentication';
  constructor(protected localStorage: LocalStorage) {}

  setData(data: AuthenticationToken){
    return this.localStorage.setItem(this.key, data);
  }

  getData():Observable<AuthenticationToken> {
    return this.localStorage.getItem(this.key);
  }

  removeData() {
    return this.localStorage.clear();
  }
}
