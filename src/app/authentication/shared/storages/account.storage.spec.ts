import { TestBed, inject } from '@angular/core/testing';

import { AccountSharedStorage } from './account.storage';

describe('AccountSharedStorage', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountSharedStorage]
    });
  });

  it('should be created', inject([AccountSharedStorage], (service: AccountSharedStorage) => {
    expect(service).toBeTruthy();
  }));
});
