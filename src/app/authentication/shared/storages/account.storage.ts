import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Observable } from 'rxjs';
import { Account } from '../models/account.model';

@Injectable()
export class AccountSharedStorage {
  private _account:Account;
  get account():Account {
    return this._account;
  }
  set account(account:Account) {
    this._account = account;
  }
}
