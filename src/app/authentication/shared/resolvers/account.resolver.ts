import { Injectable, Inject } from '@angular/core';
import { Resolve } from '@angular/router';

import { Account } from '../models/account.model';
import { AccountSharedStorage } from '../storages/account.storage';

@Injectable()
export class AccountResolver implements Resolve<Account> {
  constructor(private storage: AccountSharedStorage)
  { }
  resolve():Account {
    return this.storage.account;
  }
}
