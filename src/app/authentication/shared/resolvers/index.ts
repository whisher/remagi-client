import { AccountResolver } from './account.resolver';

export const resolvers: any[] = [
   AccountResolver
];

export * from './account.resolver';
