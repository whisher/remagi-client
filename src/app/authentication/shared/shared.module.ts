import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import * as fromGuards from './guards';
import * as fromInterceptors from './interceptors';
import * as fromResolvers from './resolvers';
import * as fromService from './services';
import * as fromStorages from './storages';

@NgModule({
  imports: [
    CommonModule
  ]
})

export class AuthenticationSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthenticationSharedModule,
      providers: [
        ...fromGuards.guards,
        ...fromInterceptors.interceptors,
        ...fromResolvers.resolvers,
        ...fromService.services,
        ...fromStorages.storages
      ]
    };
  }
}
