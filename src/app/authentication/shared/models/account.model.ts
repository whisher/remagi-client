export interface Account {
  avatar: string,
  email: string,
  hasWelcome: boolean,
  id: string,
  username: string,
}
