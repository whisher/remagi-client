export interface AuthenticationToken {
  token: string;
  expiresIn: number
}
