import { Injectable, Inject } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { AuthenticationToken } from '../models/authentication.model';
import { AuthenticationSharedStorage } from '../storages/authentication.storage';


@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private storage: AuthenticationSharedStorage) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.storage.getData()
    .pipe(
      switchMap((data:AuthenticationToken )=> {
      if(data){
        const authToken = data.token;
        const authReq = req.clone({ setHeaders: { Authorization: authToken } });
        return next.handle(authReq)
        .pipe(
          tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {

            }
          }, (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.storage.removeData().subscribe(() => {
                this.router.navigate(['/auth/login']);
              });
            }
          }
        }));
      }
      return next.handle(req);
      })
    );
  }
}
