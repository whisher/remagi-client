import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationSharedService } from './authentication.service';

describe('AuthenticationSharedService ', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationSharedService ]
    });
  });

  it('should be created', inject([AuthenticationSharedService ], (service: AuthenticationSharedService) => {
    expect(service).toBeTruthy();
  }));
});
