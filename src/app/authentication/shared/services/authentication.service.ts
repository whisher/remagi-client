import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { URLS } from '../../../config/config.tokens';
import { Account } from '../models/account.model';
import { AccountSharedStorage } from '../storages/account.storage';

@Injectable()
export class AuthenticationSharedService {
  urlAccount: string;
  constructor(
    private http: HttpClient,
    @Inject(URLS) private urls,
    private storage: AccountSharedStorage
  ) {
    this.urlAccount = urls.authentication.account;
  }

  account(): Observable<Account>{
    return this.http.get<Account>(this.urlAccount);
  }
}
