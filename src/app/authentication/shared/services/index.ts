import { AuthenticationSharedService } from './authentication.service';

export const services: any[] = [
   AuthenticationSharedService
];

export * from './authentication.service';
