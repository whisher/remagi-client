import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


import { AuthenticationSharedService } from '../services/authentication.service';
import { AccountSharedStorage } from '../storages/account.storage';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      return this.service.account()
      .pipe(
        map(data => {
          this.storage.account = data;
          return true;
        }),
        catchError(error => {
          this.router.navigate(['/auth/login']);
          return of(false);
        })
      );
  }
  constructor(
    private router: Router,
    private service: AuthenticationSharedService,
    private storage: AccountSharedStorage){}
}
