import { AuthenticationGuard } from './authentication.guard';

export const guards: any[] = [
  AuthenticationGuard
];

export * from './authentication.guard';
