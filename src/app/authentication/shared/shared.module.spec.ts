import { AuthenticationSharedModule } from './shared.module';

describe('SharedModule', () => {
  let sharedModule: AuthenticationSharedModule;

  beforeEach(() => {
    sharedModule = new AuthenticationSharedModule();
  });

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});
