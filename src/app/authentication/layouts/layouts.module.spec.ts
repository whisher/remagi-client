import { AuthenticationLayoutsModule } from './layouts.module';

describe('LayoutsModule', () => {
  let layoutsModule: AuthenticationLayoutsModule;

  beforeEach(() => {
    layoutsModule = new AuthenticationLayoutsModule();
  });

  it('should create an instance', () => {
    expect(layoutsModule).toBeTruthy();
  });
});
