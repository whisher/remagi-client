import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {
  animate,
  animateChild,
  group,
  query,
  style,
  transition,
  trigger } from '@angular/animations';

@Component({
  selector: 'iwdf-authentication-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  host:{'class': 'bg-white'},
  animations: [
    trigger('routeState', [
      transition('* => *', [
        group([
          query(':enter', [
            style({
              transform: 'translateX(-100%)'
            }),
            animate('500ms ease-out')
          ], {optional: true}),
          query(':leave', [
            animate('500ms ease-out', style({
              transform: 'translateX(100%)'
            }),)
          ], {optional: true})
        ])
      ])
    ])
  ]
})
export class AuthenticationMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getAnimationData(outlet: RouterOutlet) {
    const routeData = outlet.activatedRouteData['animation'];
    if (!routeData) {
      return 'login';
    }
    return routeData['page'];
  }
}
