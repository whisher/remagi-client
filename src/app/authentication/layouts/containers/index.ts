import { AuthenticationMainComponent } from './main/main.component';

export const containers: any[] = [
  AuthenticationMainComponent
];

export * from './main/main.component';
