import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import * as fromContainers from './containers';

@NgModule({
  imports: [
    RouterModule
  ],
  declarations: [...fromContainers.containers]
})
export class AuthenticationLayoutsModule { }
