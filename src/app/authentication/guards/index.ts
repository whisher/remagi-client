import { LoginGuard } from './login.guard';
import { MemoryGuard } from './memory.guard';

export const guards: any[] = [
  LoginGuard,
  MemoryGuard
];

export * from './login.guard';
export * from './memory.guard';
