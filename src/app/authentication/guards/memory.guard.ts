import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

import { AuthenticationMemory } from '../services/authentication.memory';


@Injectable()
export class MemoryGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const currentUrl = state.url;
      let canBeActivated = false;
      switch (currentUrl) {
        /* Company */
        case '/auth/accountinfo':
          canBeActivated = !!this.memory.select('whoareyou');
        break;
        case '/auth/accessinfo':
          canBeActivated = !!this.memory.select('basicinfo') || !!this.memory.select('accountinfo');
        break;
        case '/auth/basicinfo':
          canBeActivated = !!this.memory.select('whoareyou');
        break;
        case '/auth/companyinfo':
          canBeActivated = !!this.memory.select('whoareyou') || !!this.memory.select('companyinfo');
        break;
      }
      if(!canBeActivated){
        this.router.navigate(['/auth/login']);
      }
      return canBeActivated;

  }
  constructor(
    private router: Router,
    private memory: AuthenticationMemory)
  {}
}
