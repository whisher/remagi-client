import { TestBed, async, inject } from '@angular/core/testing';

import { MemoryGuard } from './memory.guard';

describe('AccountGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemoryGuard]
    });
  });

  it('should ...', inject([MemoryGuard], (guard: MemoryGuard) => {
    expect(guard).toBeTruthy();
  }));
});
