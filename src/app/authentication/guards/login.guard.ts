import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { AuthenticationSharedStorage } from '../shared/storages/authentication.storage';

@Injectable()
export class LoginGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      return this.storage.getData()
      .pipe(
        map(data => {
          if(data){
            this.router.navigate(['/admin/dashboard']);
            return false;
          }
          return true;
        })
      );
  }
  constructor(
    private router: Router,
    private storage: AuthenticationSharedStorage){}
}
