import { animate, group, state, style, transition, trigger, query } from '@angular/animations';

export const formStateTrigger = trigger('formState', [
  transition('* => *', [
    query('input.ng-invalid:focus', [
      animate(200, style({
        backgroundColor: '#f86280'
      })),
      animate(200)
    ], {optional: true})
  ])
]);
