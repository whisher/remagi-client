export interface AuthenticationNavigationBase {
  state: {}
}
export interface AuthenticationNavigationFreelancer extends AuthenticationNavigationBase {
  state: {
    frelancer: {
      back: Array<string>
      next: Array<string>
    }
  }
}
export interface AuthenticationNavigationCompany extends AuthenticationNavigationBase {
  state: {
    company: {
      back: Array<string>
      next: Array<string>
    }
  }
}

export interface AuthenticationNavigation extends AuthenticationNavigationBase {
  state: {
    frelancer: {
      back: Array<string>
      next: Array<string>
    }
    company: {
      back: Array<string>
      next: Array<string>
    }
  }
}

export type MenuItem = AuthenticationNavigationFreelancer | AuthenticationNavigationCompany | AuthenticationNavigation;
