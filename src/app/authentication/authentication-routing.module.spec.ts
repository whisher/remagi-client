import { AuthenticationRoutingModule } from './authentication-routing.module';

describe('AuthenticationModule', () => {
  let authenticationRoutingModule: AuthenticationRoutingModule;

  beforeEach(() => {
    authenticationRoutingModule = new AuthenticationRoutingModule();
  });

  it('should create an instance', () => {
    expect(authenticationRoutingModule).toBeTruthy();
  });
});
