import { NgModule } from '@angular/core';

import { AppSharedModule } from '../app-shared.module';

import { AuthenticationLayoutsModule } from './layouts/layouts.module';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { IwdfUiButtonSpinnerModule } from '../ui/button-spinner/button-spinner.module';
import { IwdfUiPasswordModule } from '../ui/password/password.module';

import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromServices from './services';

@NgModule({
  imports: [
    AppSharedModule,
    AuthenticationLayoutsModule,
    AuthenticationRoutingModule,
    IwdfUiButtonSpinnerModule,
    IwdfUiPasswordModule
  ],
  declarations: [
    ...fromComponents.components,
    ...fromContainers.containers
  ],
  providers: [
    ...fromServices.services
  ]
})
export class AuthenticationModule { }
