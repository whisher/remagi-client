import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationMemory } from './authentication.memory';

describe('AuthenticationMemory', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationMemory]
    });
  });

  it('should be created', inject([AuthenticationMemory], (service: AuthenticationMemory) => {
    expect(service).toBeTruthy();
  }));
});
