import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationMemory {

  _data: any = {};

  get data() {
    return this._data;
  }

  set(data: any) {
    this._data = {...this._data, ...data};
  }

  select(key: string) {
    if(key in this._data) {
      return this._data[key];
    }
    return null;
  }

}
