import { AuthenticationMemory } from './authentication.memory';
import { AuthenticationService } from './authentication.service';

export const services: any[] = [
  AuthenticationMemory,
  AuthenticationService
];

export * from './authentication.service';
export * from './authentication.memory';
