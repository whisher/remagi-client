import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AuthenticationLogin } from '../models/authentication.model';
import { AuthenticationToken } from '../shared/models/authentication.model';
import { URLS } from '../../config/config.tokens';

@Injectable()
export class AuthenticationService {
  urlLogin: string;
  urlSignup: string;
  constructor(
    private http: HttpClient,
    @Inject(URLS) private urls
  ) {
    this.urlLogin = urls.authentication.login;
    this.urlSignup = urls.authentication.signup;
  }

  login(data: AuthenticationLogin) {
    return this.http.post<AuthenticationToken>(this.urlLogin, data);
  }

  signup(data) {
    return this.http.post<AuthenticationToken>(this.urlSignup, data);
  }
}
