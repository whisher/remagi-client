import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationBasicInfoComponent } from './basicinfo.component';

describe('AuthenticationBasicInfoComponent', () => {
  let component: AuthenticationBasicInfoComponent;
  let fixture: ComponentFixture<AuthenticationBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
