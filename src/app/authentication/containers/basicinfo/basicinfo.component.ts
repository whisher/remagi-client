import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationNavigationFreelancer } from '../../models/navigation.model';
import { AuthenticationMemory } from '../../services/authentication.memory';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-basicinfo',
  templateUrl: './basicinfo.component.html',
  styleUrls: ['./basicinfo.component.scss']
})
export class AuthenticationBasicInfoComponent implements OnInit {

  role = 'frelancer';

  numOfSteps: number;

  step = 1;

  currentNav: AuthenticationNavigationFreelancer = {
    state: {
      frelancer: {
        back:['/auth/whoareyou'],
        next: ['/auth/accessinfo']
      }
    }
  };

  frm: FormGroup;

  data: any;


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private memory: AuthenticationMemory
  ) {
    this.numOfSteps = 3;
    this.createForm();
  }

  ngOnInit() {
    const basicinfo = this.memory.select('basicinfo');
    if(basicinfo){
      this.frm.setValue(basicinfo);
    }
  }

  createForm() {
    this.frm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2)]],
      lastname: ['', [Validators.required, Validators.minLength(2)]],
      city: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  onBack(event){
    this.router.navigate(this.currentNav.state.frelancer.back);
  }

  onNext(event){
    const data = {
      basicinfo: this.frm.value
    };
    this.data = data;
    this.memory.set(this.data);
    this.router.navigate(this.currentNav.state.frelancer.next);
  }

}
