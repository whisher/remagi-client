import { AuthenticationAccessInfoComponent } from './accessinfo/accessinfo.component';
import { AuthenticationAccountInfoComponent } from './accountinfo/accountinfo.component';
import { AuthenticationBasicInfoComponent } from './basicinfo/basicinfo.component';
import { AuthenticationCompanyInfoComponent } from './companyinfo/companyinfo.component';
import { AuthenticationForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthenticationLoginComponent } from './login/login.component';
import { AuthenticationLogoutComponent } from './logout/logout.component';
import { AuthenticationWhoAreYouComponent } from './whoareyou/whoareyou.component';

export const containers: any[] = [
  AuthenticationAccessInfoComponent,
  AuthenticationAccountInfoComponent,
  AuthenticationBasicInfoComponent,
  AuthenticationCompanyInfoComponent,
  AuthenticationForgotPasswordComponent,
  AuthenticationLoginComponent,
  AuthenticationLogoutComponent,
  AuthenticationWhoAreYouComponent
];

export * from './accessinfo/accessinfo.component';
export * from './accountinfo/accountinfo.component';
export * from './basicinfo/basicinfo.component';
export * from './companyinfo/companyinfo.component';
export * from './forgot-password/forgot-password.component';
export * from './login/login.component';
export * from './logout/logout.component';
export * from './whoareyou/whoareyou.component';
