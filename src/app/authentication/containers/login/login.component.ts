import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { formStateTrigger } from '../../animations';
import { AuthenticationService } from '../../services/authentication.service';
import { AuthenticationToken } from '../../shared/models/authentication.model';
import { AuthenticationSharedStorage } from '../../shared/storages/authentication.storage';

@Component({
  selector: 'iwdf-authentication-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  host: {'class': 'd-block'},
  animations: [formStateTrigger]
})
export class AuthenticationLoginComponent  {
  frm: FormGroup;
  hasError = false;
  hasSubmitted = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service: AuthenticationService,
    private storage: AuthenticationSharedStorage
  ) {
    this.createForm();
  }

  createForm() {
    this.frm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.frm.valid) {
      this.hasSubmitted = true;
      this.service.login(this.frm.value)
      .subscribe(
        (data: AuthenticationToken) => {
          this.storage.setData(data)
          .subscribe(() => {
            this.router.navigate(['/admin/dashboard']);
          });
        },
        error => {
          this.hasSubmitted = false;
          this.hasError = true;
        }
      );
    }
  }

}
