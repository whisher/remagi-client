import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationNavigation } from '../../models/navigation.model';
import { AuthenticationToken } from '../../shared/models/authentication.model';
import { AuthenticationMemory } from '../../services/authentication.memory';
import { AuthenticationService } from '../../services/authentication.service';
import { AuthenticationSharedStorage } from '../../shared/storages/authentication.storage';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-accessinfo',
  templateUrl: './accessinfo.component.html',
  styleUrls: ['./accessinfo.component.scss']
})
export class AuthenticationAccessInfoComponent implements OnInit {

  role: string;

  numOfSteps: number;

  step: number;

  currentNav: AuthenticationNavigation = {
    state: {
      frelancer: {
        back:['/auth/basicinfo'],
        next: null
      },
      company: {
        back:['/auth/accountinfo'],
        next: ['/auth/companyinfo']
      }
    }
  };

  frm: FormGroup;

  data: any;

  isValid = false;

  hasSubmitted = false;

  hasError = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private memory: AuthenticationMemory,
    private service: AuthenticationService,
    private storage: AuthenticationSharedStorage) {
      this.role = memory.select('whoareyou').role;
      if(this.role === 'freelancer') {
        this.numOfSteps = 3;
        this.step = 2;
      }
      else{
        this.numOfSteps = 4;
        this.step = 2;
      }
      this.createForm();
  }

  ngOnInit() {
    const accessinfo = this.memory.select('accessinfo');
    if(accessinfo){
      this.frm.setValue(accessinfo);
    }
  }

  createForm() {
    this.frm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onBack(event){
    if(this.role === 'freelancer') {
      this.router.navigate(this.currentNav.state.frelancer.back);
    } else {
      this.router.navigate(this.currentNav.state.company.back);
    }
  }

  onNext(event){
    const formData = {
      email: this.frm.value.email,
      password: this.frm.value.password,
      username: this.frm.value.username,
    };
    if(this.role === 'freelancer') {
      const {whoareyou, basicinfo} = this.memory.data;
      const data = {...whoareyou, ...basicinfo, ...formData};
      this.service.signup(data)
      .subscribe(
        (data: AuthenticationToken) => {
          this.storage.setData(data)
          .subscribe(() => {
            this.router.navigate(['/admin/dashboard']);
          });
        },
        error => {
          this.hasSubmitted = false;
          this.hasError = true;
        }
      );
    } else {
      const data = {
        accessinfo: {...formData}
      };
      this.memory.set(data);
      this.router.navigate(this.currentNav.state.company.next);
    }
  }

}
