import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationAccessInfoComponent } from './accessinfo.component';

describe('AuthenticationAccessInfoComponent', () => {
  let component: AuthenticationAccessInfoComponent;
  let fixture: ComponentFixture<AuthenticationAccessInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationAccessInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationAccessInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
