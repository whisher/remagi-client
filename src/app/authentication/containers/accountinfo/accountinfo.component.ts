import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationNavigationCompany } from '../../models/navigation.model';
import { AuthenticationMemory } from '../../services/authentication.memory';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-accountinfo',
  templateUrl: './accountinfo.component.html',
  styleUrls: ['./accountinfo.component.scss']
})
export class AuthenticationAccountInfoComponent implements OnInit {

  role = 'company';

  numOfSteps: number;

  step: number;

  currentNav: AuthenticationNavigationCompany = {
    state: {
      company: {
        back:['/auth/whoareyou'],
        next: ['/auth/accessinfo']
      }
    }
  };

  frm: FormGroup;

  data: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private memory: AuthenticationMemory
  ) {
    this.numOfSteps = 4;
    this.step = 1;
    this.createForm();
  }

  ngOnInit() {
    const companyinfo = this.memory.select('accountinfo');
    if(companyinfo){
      this.frm.setValue(companyinfo);
    }
  }

  createForm() {
    this.frm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2)]],
      lastname: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  onBack(event){
    this.router.navigate(this.currentNav.state.company.back);
  }

  onNext(event){
    const data = {
      accountinfo: this.frm.value
    };
    this.data = data;
    this.memory.set(this.data);
    this.router.navigate(this.currentNav.state.company.next);
  }

}
