import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationAccountInfoComponent } from './accountinfo.component';

describe('AuthenticationAccountInfoComponent', () => {
  let component: AuthenticationAccountInfoComponent;
  let fixture: ComponentFixture<AuthenticationAccountInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationAccountInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationAccountInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
