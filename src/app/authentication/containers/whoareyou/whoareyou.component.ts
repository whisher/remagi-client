import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationNavigation } from '../../models/navigation.model';
import { AuthenticationMemory } from '../../services/authentication.memory';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-whoareyou',
  templateUrl: './whoareyou.component.html',
  styleUrls: ['./whoareyou.component.scss']
})
export class AuthenticationWhoAreYouComponent implements OnInit {

  role: string;

  numOfSteps: number;

  currentNav: AuthenticationNavigation = {
    state: {
      frelancer: {
        back:['/auth/login'],
        next: ['/auth/basicinfo']
      },
      company: {
        back:['/auth/login'],
        next: ['/auth/accountinfo']
      }
    }
  };

  isValid = false;

  data: any;

  constructor(
    private router: Router,
    private memory: AuthenticationMemory
  ) {
    this.numOfSteps = 3;
    const whoareyou = memory.select('whoareyou');
    if(whoareyou){
      this.role = whoareyou.role;
      this.isValid = true;
      if(this.role === 'company') {
        this.numOfSteps = 4;
      } else {
        this.numOfSteps = 3;
      }
    }
  }

  ngOnInit() {}

  onSelect(role){
    this.isValid = true;
    this.role = role;
    if(this.role === 'company') {
      this.numOfSteps = 4;
    } else {
      this.numOfSteps = 3;
    }
  }

  onBack(event){
    if(this.role === 'freelancer') {
      this.router.navigate(this.currentNav.state.frelancer.back);
    } else {
      this.router.navigate(this.currentNav.state.company.back);
    }
  }

  onNext(event){
    const data = {
      whoareyou: { role: this.role }
    };
    this.data = data;
    this.memory.set(this.data);
    if(this.role === 'freelancer'){
      this.router.navigate(this.currentNav.state.frelancer.next);
    } else {
      this.router.navigate(this.currentNav.state.company.next);
    }
  }
}
