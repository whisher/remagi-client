import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationWhoAreYouComponent } from './whoareyou.component';

describe('AuthenticationWhoAreYouComponent', () => {
  let component: AuthenticationWhoAreYouComponent;
  let fixture: ComponentFixture<AuthenticationWhoAreYouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationWhoAreYouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationWhoAreYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
