import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationNavigationCompany } from '../../models/navigation.model';
import { AuthenticationToken } from '../../shared/models/authentication.model';
import { AuthenticationMemory } from '../../services/authentication.memory';
import { AuthenticationService } from '../../services/authentication.service';
import { AuthenticationSharedStorage } from '../../shared/storages/authentication.storage';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-companyinfo',
  templateUrl: './companyinfo.component.html',
  styleUrls: ['./companyinfo.component.scss']
})
export class AuthenticationCompanyInfoComponent implements OnInit {

  role = 'company';

  numOfSteps: number;

  step: number;

  currentNav: AuthenticationNavigationCompany = {
    state: {
      company: {
        back:['/auth/accountinfo'],
        next: null
      }
    }
  };

  frm: FormGroup;

  data: any;

  hasSubmitted = false;

  hasError = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private memory: AuthenticationMemory,
    private service: AuthenticationService,
    private storage: AuthenticationSharedStorage
  ) {
    this.numOfSteps = 4;
    this.step = 3;
    this.createForm();
  }

  ngOnInit() {
    const companyinfo = this.memory.select('companyinfo');
    if(companyinfo){
      this.frm.setValue(companyinfo);
    }
  }

  createForm() {
    this.frm = this.fb.group({
      company_name: ['', Validators.required],
      company_website: [''],
      company_city: ['', Validators.required]
    });
  }

  onBack(event){
    this.router.navigate(this.currentNav.state.company.back);
  }

  onNext(event) {
    const {whoareyou, accountinfo, accessinfo} = this.memory.data;
    const data = {...whoareyou, ...accountinfo, ...accessinfo, ...this.frm.value};
    this.service.signup(data)
    .subscribe(
      (data: AuthenticationToken) => {
        this.storage.setData(data)
        .subscribe(() => {
          this.router.navigate(['/admin/dashboard']);
        });
      },
      error => {
        this.hasSubmitted = false;
        this.hasError = true;
      }
    );
  }

}
