import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationCompanyInfoComponent } from './companyinfo.component';

describe('AuthenticationCompanyInfoComponent', () => {
  let component: AuthenticationCompanyInfoComponent;
  let fixture: ComponentFixture<AuthenticationCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
