import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationSharedStorage } from '../../shared/storages/authentication.storage';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
  host: {'class': 'bg-warning'}
})
export class AuthenticationLogoutComponent implements OnInit {

  constructor(
    private router: Router,
    private storage: AuthenticationSharedStorage) { }

  ngOnInit() {
    this.storage.removeData()
    .subscribe(() => {
      this.router.navigate(['/auth/login']);
    })
  }

}
