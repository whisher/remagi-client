import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationForgotPasswordComponent } from './forgot-password.component';

describe('AuthenticationForgotPasswordComponent', () => {
  let component: AuthenticationForgotPasswordComponent;
  let fixture: ComponentFixture<AuthenticationForgotPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationForgotPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
