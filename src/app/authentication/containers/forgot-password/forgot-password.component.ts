import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../services/authentication.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-authentication-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class AuthenticationForgotPasswordComponent {

  frm: FormGroup;
  hasSubmitted = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private service: AuthenticationService) {
    this.createForm();
  }

  createForm() {
    this.frm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  onSubmit(){

  }
}
