import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';

import { Profile } from './admin/features/profile/models/profile';

export interface State {
  profile: Profile
}

const state: State = {
  profile: undefined
};

@Injectable({
  providedIn: 'root'
})
export class AppStore {
  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable()
    .pipe(distinctUntilChanged());

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store
      .pipe(pluck(name));
  }

  set(name: string, state: any) {
    this.subject.next({ ...this.value, [name]: state });
  }

}
