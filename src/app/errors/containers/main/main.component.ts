import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'iwdf-errors-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  host: {'class': 'bg-danger'}
})
export class ErrorsMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
