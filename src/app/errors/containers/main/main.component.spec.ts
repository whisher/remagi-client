import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsMainComponent } from './main.component';

describe('ErrorsMainComponent', () => {
  let component: ErrorsMainComponent;
  let fixture: ComponentFixture<ErrorsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
