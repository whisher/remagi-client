import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorsMainComponent } from './containers';



export const ROUTES: Routes = [
  {
    path: '', component: ErrorsMainComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class ErrorsRoutingModule { }
