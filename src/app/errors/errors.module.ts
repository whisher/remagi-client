import { NgModule } from '@angular/core';

import { AppSharedModule } from '../app-shared.module';

import { ErrorsRoutingModule } from './errors-routing.module';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    AppSharedModule,
    ErrorsRoutingModule
  ],
  declarations: [...fromContainers.containers]
})
export class ErrorsModule { }
