import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'admin/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path: 'auth',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  },
  {
    path: 'default',
    loadChildren: './default/default.module#DefaultModule'
  },
  {
    path: 'not-found',
    loadChildren: './errors/errors.module#ErrorsModule',
    data: { message: 'Page not found!' }
  },
  {
    path: '**', redirectTo: '/not-found'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
