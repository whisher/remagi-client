import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';

import * as moment from 'moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppGlobalErrorHandler } from './app-global-error.handler';

import { URLS } from './config/config.tokens';
import { Config_Urls } from './config/config-urls';

import { AppRoutingModule } from './app-routing.module';
import { AuthenticationSharedModule } from './authentication/shared/shared.module';
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule.withServerTransition({appId: 'remagi'}),
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AuthenticationSharedModule.forRoot(),
    AppRoutingModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    { provide: ErrorHandler, useClass: AppGlobalErrorHandler },
    { provide: URLS, useValue: Config_Urls },
    { provide: 'moment', useFactory: (): any => moment }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
