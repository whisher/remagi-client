export interface ConfigUrls {
  authentication: {
    account: string;
    login: string;
    logout: string;
    signup: string;
  },
  user: {
    profile: string
  }
}
