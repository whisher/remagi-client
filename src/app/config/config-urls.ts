import { environment } from '../../environments/environment';
import { ConfigUrls } from './config-urls.interface';

const host = environment.production ? 'http://localhost:3000' : 'http://localhost:3000';

export function urls(): ConfigUrls {
  return {
    authentication: {
      account: `${host}/users/account`,
      login: `${host}/users/login`,
      logout: `${host}/users/logout`,
      signup: `${host}/users/signup`
    },
    user: {
      profile: `${host}/users/profile` 
    }
  };
}

export const Config_Urls: ConfigUrls = urls();
