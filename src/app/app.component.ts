import { Component } from '@angular/core';

@Component({
  selector: 'iwdf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
