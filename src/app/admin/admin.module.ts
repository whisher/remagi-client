import { NgModule } from '@angular/core';

import { AdminLayoutsModule } from './layouts/layouts.module';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  imports: [
    AdminLayoutsModule,
    AdminRoutingModule
  ],
  declarations: []
})
export class AdminModule { }
