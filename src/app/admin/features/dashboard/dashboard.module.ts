import { NgModule } from '@angular/core';

import { AdminDashboardRoutingModule } from './dashboard-routing.module';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    AdminDashboardRoutingModule
  ],
  declarations: [...fromContainers.containers]
})
export class AdminDashboardModule { }
