import { AdminDashboardMainComponent } from './dashboard-main/dashboard-main.component';

export const containers: any[] = [
   AdminDashboardMainComponent
];

export * from './dashboard-main/dashboard-main.component';
