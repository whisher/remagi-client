import { NgModule } from '@angular/core';

import { AppSharedModule } from '../../../app-shared.module';
import { IwdfUiLoaderModule } from '../../../ui/loader/loader.module';
import { AdminProfileRoutingModule } from './profile-routing.module';

import * as fromContainers from './containers';
import * as fromServices from './services';

@NgModule({
  imports: [
    AppSharedModule,
    IwdfUiLoaderModule,
    AdminProfileRoutingModule
  ],
  declarations: [
    ...fromContainers.containers
  ],
  providers: [
    ...fromServices.services
  ]
})
export class AdminProfileModule { }
