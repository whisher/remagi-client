import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Observable} from 'rxjs';

import { URLS } from '../../../../config/config.tokens';
import { AppStore } from '../../../../app.store';
import { Profile } from '../models/profile';

@Injectable()
export class ProfileService {
  urlProfile: string;
  constructor(
    private http: HttpClient,
    @Inject(URLS) private urls,
    private store: AppStore
  ) {
    this.urlProfile = urls.user.profile;
  }

  profile() {
    this.http.get<Profile>(this.urlProfile)
      .subscribe((profile: Profile) => {
        this.store.set('profile',profile);
      });
  }

  selectProfile(): Observable<Profile>{
    this.profile();
    return this.store.select<Profile>('profile');
  }
}
