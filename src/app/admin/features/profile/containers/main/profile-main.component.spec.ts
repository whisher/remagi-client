import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProfileMainComponent } from './profile-main.component';

describe('ProfileMainComponent', () => {
  let component: AdminProfileMainComponent;
  let fixture: ComponentFixture<AdminProfileMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProfileMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfileMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
