import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Profile } from '../../models/profile';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'iwdf-admin-profile-main',
  templateUrl: './profile-main.component.html',
  styleUrls: ['./profile-main.component.scss']
})
export class AdminProfileMainComponent implements OnInit {
  profile$: Observable<Profile>;
  constructor(private service: ProfileService) {}

  ngOnInit() {
    this.profile$ = this.service.selectProfile();
  }

}
