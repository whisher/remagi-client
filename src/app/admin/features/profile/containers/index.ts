import { AdminProfileMainComponent } from './main/profile-main.component';

export const containers: any[] = [
   AdminProfileMainComponent
];

export * from './main/profile-main.component';
