import { AdminProfileModule } from './profile.module';

describe('AdminProfileModule', () => {
  let profileModule: AdminProfileModule;

  beforeEach(() => {
    profileModule = new AdminProfileModule();
  });

  it('should create an instance', () => {
    expect(profileModule).toBeTruthy();
  });
});
