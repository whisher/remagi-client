import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminProfileMainComponent } from './containers';

export const ROUTES: Routes = [
  {
    path: '', component: AdminProfileMainComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class AdminProfileRoutingModule {}
