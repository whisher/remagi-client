export interface Profile {
  avatar: string,
  city: string,
  company_name: string,
  company_city: string,
  company_website: string,
  email: string,
  firstname: string,
  hasWelcome: string,
  lastname: string,
  role: string,
  username: string
}
