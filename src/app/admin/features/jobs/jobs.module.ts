import { NgModule } from '@angular/core';

import { AdminJobsRoutingModule } from './jobs-routing.module';

import * as fromContainers from './containers';

@NgModule({
  imports: [
    AdminJobsRoutingModule
  ],
  declarations: [...fromContainers.containers]
})
export class AdminJobsModule { }
