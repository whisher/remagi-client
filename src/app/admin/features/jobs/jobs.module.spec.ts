import { AdminJobsModule } from './jobs.module';

describe('AdminJobsModule', () => {
  let jobsModule: AdminJobsModule;

  beforeEach(() => {
    jobsModule = new AdminJobsModule();
  });

  it('should create an instance', () => {
    expect(jobsModule).toBeTruthy();
  });
});
