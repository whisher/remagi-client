import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminJobsMainComponent } from './containers';

const ROUTES: Routes = [
  {
    path: '', component: AdminJobsMainComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class AdminJobsRoutingModule {}
