import { AdminJobsMainComponent } from './jobs-main/jobs-main.component';

export const containers: any[] = [
   AdminJobsMainComponent
];

export * from './jobs-main/jobs-main.component';
