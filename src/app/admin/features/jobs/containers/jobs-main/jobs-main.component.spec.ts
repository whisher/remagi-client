import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminJobsMainComponent } from './jobs-main.component';

describe('JobsMainComponent', () => {
  let component: AdminJobsMainComponent;
  let fixture: ComponentFixture<AdminJobsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminJobsMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminJobsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
