import { AdminProjectsMainComponent } from './projects-main/projects-main.component';

export const containers: any[] = [
   AdminProjectsMainComponent
];

export * from './projects-main/projects-main.component';
