import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProjectsMainComponent } from './projects-main.component';

describe('ProjectsMainComponent', () => {
  let component: AdminProjectsMainComponent;
  let fixture: ComponentFixture<AdminProjectsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProjectsMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
