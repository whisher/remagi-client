import { AdminProjectsModule } from './projects.module';

describe('AdminProjectsModule', () => {
  let projectsModule: AdminProjectsModule;

  beforeEach(() => {
    projectsModule = new AdminProjectsModule();
  });

  it('should create an instance', () => {
    expect(projectsModule).toBeTruthy();
  });
});
