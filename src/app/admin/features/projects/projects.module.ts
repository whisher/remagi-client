import { NgModule } from '@angular/core';

import { AdminProjectsRoutingModule } from './projects-routing.module';

import * as fromContainers from './containers';

@NgModule({
  imports: [
    AdminProjectsRoutingModule
  ],
  declarations: [...fromContainers.containers]
})
export class AdminProjectsModule { }
