import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminProjectsMainComponent } from './containers';

const ROUTES: Routes = [
  {
    path: '', component: AdminProjectsMainComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class AdminProjectsRoutingModule {}
