import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeaderMainComponent } from './main.component';

describe('AdminHeaderMainComponent', () => {
  let component: AdminHeaderMainComponent;
  let fixture: ComponentFixture<AdminHeaderMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeaderMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeaderMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
