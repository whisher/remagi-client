import { Component, OnInit, Input } from '@angular/core';

import { Account } from '../../../../../../authentication/shared/models/account.model';

@Component({
  selector: 'iwdf-admin-header-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class AdminHeaderMainComponent implements OnInit {
  @Input() account: Account;
  constructor() {}
  ngOnInit() {}
}
