import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'iwdf-admin-header-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class AdminHeaderNavComponent {
  public links = [{
      label: 'Dashboard',
      url: '/admin/dashboard',
    },{
      label: 'Projects',
      url: '/admin/projects',
    },{
      label: 'Jobs',
      url: '/admin/jobs',
    },{
      label: 'Profile',
      url: '/admin/profile',
    }
  ];
}
