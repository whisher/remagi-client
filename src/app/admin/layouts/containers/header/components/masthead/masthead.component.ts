import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Account } from '../../../../../../authentication/shared/models/account.model';

@Component({
  selector: 'iwdf-admin-header-masthead',
  templateUrl: './masthead.component.html',
  styleUrls: ['./masthead.component.scss']
})
export class AdminHeaderMastheadComponent implements OnInit {
  @Input() account: Account;
  constructor(private router: Router) { }
  ngOnInit() {}
  onLogout(){
    this.router.navigate(['/auth/logout']);
  }
}
