import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeaderMastheadComponent } from './masthead.component';

describe('AdminHeaderMastheadComponent', () => {
  let component: AdminHeaderMastheadComponent;
  let fixture: ComponentFixture<AdminHeaderMastheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeaderMastheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeaderMastheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
