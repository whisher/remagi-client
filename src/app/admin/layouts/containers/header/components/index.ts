import { AdminHeaderBarComponent } from './bar/bar.component';
import { AdminHeaderBrandComponent } from './brand/brand.component';
import { AdminHeaderMastheadComponent } from './masthead/masthead.component';
import { AdminHeaderNavComponent } from './nav/nav.component';

export const components: any[] = [
   AdminHeaderBarComponent,
   AdminHeaderBrandComponent,
   AdminHeaderMastheadComponent,
   AdminHeaderNavComponent
];
