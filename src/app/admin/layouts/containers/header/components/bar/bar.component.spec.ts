import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeaderBarComponent } from './bar.component';

describe('AdminHeaderBarComponent', () => {
  let component: AdminHeaderBarComponent;
  let fixture: ComponentFixture<AdminHeaderBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeaderBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeaderBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
