import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeaderBrandComponent } from './brand.component';

describe('AdminHeaderBrandComponent', () => {
  let component: AdminHeaderBrandComponent;
  let fixture: ComponentFixture<AdminHeaderBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeaderBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeaderBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
