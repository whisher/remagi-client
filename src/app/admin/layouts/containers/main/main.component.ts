import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Account } from '../../../../authentication/shared/models/account.model';

@Component({
  selector: 'iwdf-admin-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  host: {class:'bg-secondary'}
})
export class AdminMainComponent implements OnInit {
  account: Account;
  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    this.account = this.route.snapshot.data.account;
  }

}
