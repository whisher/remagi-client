import { AdminFooterComponent } from './footer/footer.component';
import { AdminMainComponent } from './main/main.component';

export const containers: any[] = [
  AdminFooterComponent,
  AdminMainComponent
];

export * from './footer/footer.component';
export * from './main/main.component';
