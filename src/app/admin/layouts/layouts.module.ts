import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IwdfDropdownModule } from '../../ui/dropdown/dropdown.module';
import { AppSharedModule } from '../../app-shared.module';
import { AuthenticationSharedModule } from '../../authentication/shared/shared.module';

import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromHeaderComponents from './containers/header/components';
import * as fromHeaderContainers from './containers/header/containers';

@NgModule({
  imports: [
    RouterModule,
    IwdfDropdownModule,
    AppSharedModule,
    AuthenticationSharedModule
  ],
  declarations: [
    ...fromComponents.components,
    ...fromContainers.containers,
    ...fromHeaderComponents.components,
    ...fromHeaderContainers.containers,
  ]
})
export class AdminLayoutsModule { }
