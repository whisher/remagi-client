import { Component, OnInit, Input } from '@angular/core';

import { Account } from '../../../../authentication/shared/models/account.model';

@Component({
  selector: 'iwdf-admin-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class AdminWelcomeComponent implements OnInit {
  @Input() account: Account;
  hasWelcome = true;
  constructor() { }
  ngOnInit() {}
  hide(){
    this.hasWelcome = false;
  }

}
