import { AdminLayoutsModule } from './layouts.module';

describe('LayoutsModule', () => {
  let layoutsModule: AdminLayoutsModule;

  beforeEach(() => {
    layoutsModule = new AdminLayoutsModule();
  });

  it('should create an instance', () => {
    expect(layoutsModule).toBeTruthy();
  });
});
