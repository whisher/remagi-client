import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminMainComponent } from './layouts/containers';
import { AccountResolver } from '../authentication/shared/resolvers';
import { AuthenticationGuard } from '../authentication/shared/guards';

export const ROUTES: Routes = [
  {
    path: '',
    component: AdminMainComponent,
    canActivate: [AuthenticationGuard],
    resolve: {
      account: AccountResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        loadChildren: './features/dashboard/dashboard.module#AdminDashboardModule'
      },
      {
        path: 'jobs',
        loadChildren: './features/jobs/jobs.module#AdminJobsModule'
      },
      {
        path: 'profile',
        loadChildren: './features/profile/profile.module#AdminProfileModule'
      },
      {
        path: 'projects',
        loadChildren: './features/projects/projects.module#AdminProjectsModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
